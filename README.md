# Interview questions for position as Research Assistant
## Part 1
##### Coding
Siehe app.js und index.html
##### What privacy issues are related to this feature?
In diesem Fall fielen mir zunächst zwei wesentliche ein:
 - http anstelle von https
 - fehlende cross-site request forgery (CSRF)
##### Why is health data such as this especially protected by the data protection and privacy regulations (Datenschutz-Grundverordnung)?
Die Nutzer müssen über das Aufzeichnen Ihrer Daten in Kenntnis gesetzt werden. Darüberhinaus dürfen diese Daten nicht ohne Weiters an Dritte weitergegeben werden.

## Part 2
##### What is the output of the following JavaScript code snippet?
```javascript
	Promise.resolve(1)
	  .then((x) => x + 1)
	  .then((x) => {throw new Error('My Error') })
	  .catch(() => 1)
	  .then((x) => x + 1)
	  .then((x) => console.log(x))
	  .catch(console.error)
```
Das Ergebnis ist 2. Da ein `Error` erzeugt wird und danach gleich abgefangen, fängt nach dem Abfangen eine neue Kette an.

##### When using third-party (npm) packages, how do you keep them up-to-date?
Mit dem Befehl `npm help update` erhält man eine Übersicht zur Updatefunktionalität von npm.
 
##### Imagine you want to update a package from v1.2.4 to v2.0.5. What do you need to consider first?
Mit ` npm install <package>@2.0.5 ` wird die gewünschte Version des Packets installiert. Sofern das Packet bereits in der entwickelten Software Verwendung findet, sollte beachtet werden, ob dessen API noch der gleichen Funktionalität entspricht.

##### What is the main difference between AngularJS and NodeJS?
NodeJS wird eher als BackEnd verwendet wogegen AngularJS die FrontEnd-Entwicklung erleichtert.
## Part 3: Server administration
##### Please explain how file permissions work in Linux. What different kinds of permissions are there?
Die drei Datei-Zugriffs-Rechte sind Lesen(r), Schreiben(w) und Ausführen(x).
`ls -l` im Terminal zeigt die jeweiligen Zugriffsrechte der Dateien im aktuellen Verzeichnis auf.
Dabei wird berücksichtigt, dass der Nutzer(u), seine Gruppe(g) und alle Anderen(o) jeweils Rechte zugewiesen bekommen können.
##### How are they assigned to a file or directory?
` chmod g+x <filename> ` fügt beispielsweise das Recht zum Ausführen einer Datei der Gruppe hinzu.

##### On a Linux system, how can you find out if a web server is running?
Dazu gäbe es mehrere Möglichkeiten:
 1. Überprüfung der aktuell laufenden Prozesse mit `top` oder `htop`
 2. mit `ping <webserver>` gibt ein Signal wieder, sofern diese Funktion nicht unterdrückt wurde.
 2. `telnet <webServerAdresse> <port>` baut eine Verbindung zum bekannten Webserver mittels seiner Adresse auf
##### Name some HTTP request methods and briefly explain their meaning.
 - **GET** ist die üblichste Anfrage an einen Server. Dabei sind anfragespezifische Werte in der URL erkennbar. Diese Anfrage ist längen limitiert und kann auch als Lesezeichen abgespeichert bzw. auch an Andere verschickt werden.
 - **POST** sended eine Anfrage mit in der URL nicht erkennbaren Werten. Hierbei ist die Größe der übermittelten Werte innerhalb der Anfrage nicht limitiert. Diese können nicht als Lesezeichen abgespeichert werden.

Weitere Anfragen sind Abwandlungen dieser beiden.

##### In HTTPS, how is the identity of a server authenticated?
Der Server besitzt ein Zertifikat, mit welchem die Anfrage eines Nutzer verifiziert/signiert wird.
Das geschieht mithilfe eines öffentlichen Schlüssels, der auch dem Nutzer bekannt ist. Mithilfe dieses öffentlichen Schlüssels werden die empfangenen Daten beim Nutzer entschlüsselt und auf deren Unverfälschtheit geprüft.

## Part 4: Git
##### In a git repository, what is a branch? Why are they used?
Ein *branch* ist eine Abzweigung des Entwicklungsprozesses eines *repository* an einem bestimmten *commit*. An dieser Abzweigung können separat, ohne den Haupt-*branch* zu beeinflussen, weitere Features/Funktionalität implementiert werden. Dadurch lässt sich zum einen besser simultan an einem *Repository* programmieren und beim Zusammentragen (*mergen*) können Fehler ggf. den neuimplementierten Features zugeordnet werden.

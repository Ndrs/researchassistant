// database.js

const sqlite3 = require('sqlite3').verbose()
const Promise = require('bluebird')

class Database {
  constructor() {
    this.db = new sqlite3.Database(':memory:', (err) => {
      if (err) {
        console.log('Could not connect to database', err)
      } else {
        console.log('Connected to database')
      }
    })
  }
  run(sql, params = []) {
    return new Promise((resolve, reject) => {
      this.db.run(sql, params, function (err) {
        if (err) {
          console.log('Error running sql ' + sql)
          console.log(err)
          reject(err)
        } else {
          resolve({ id: this.lastID })
        }
      })
    })
  }
  createTable() {
    const sql = `
    CREATE TABLE IF NOT EXISTS appointments (
      id INTEGER PRIMARY KEY AUTOINCREMENT,
      patient TEXT,
      purpose TEXT,
      time TIMESTAMP)`
    return this.run(sql)
  }
  addAppointment(patient, purpose, time){
    return this.run('INSERT INTO appointments (patient, purpose, time) Values (?, ?, ?)',
      [ patient, purpose, time])
  }
  all() {
    return new Promise((resolve, reject) => {
      this.db.all('SELECT * FROM appointments ORDER BY time ASC', (err, rows) => {
        if (err) {
          console.log('Error running sql: ' + sql)
          console.log(err)
          reject(err)
        } else {
          resolve(rows)
        }
      })
    })
  }
}

module.exports = Database;

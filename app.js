const path = require('path')
const express = require('express')
const app = express()
const pug = require('pug')
const database = require('./database')
const db = new database()
db.createTable()

app.use(
    express.urlencoded({
        extended: true
    })
)

app.set('view engine', 'pug')

app.get('/', function (req, res){
    res.sendFile(path.join(__dirname+'/index.html'))
    res.render('index')
})

app.get('/overview', function (req, res){
    db.all().then( (d) => res.render('overview',{data: d}))
})

app.post('/overview', function(req, res){
    console.log(req.body)
    db.addAppointment(req.body.patient_name, req.body.purpose_of_appointment, req.body.time)
	.then(() => res.redirect('/overview'))
})

var server = app.listen(8080)
